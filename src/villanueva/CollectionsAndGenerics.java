package villanueva;

import java.util.*;

public class CollectionsAndGenerics {
    public static void main(String[] args){

        try{ // Exception handler

            // Creates a few Student objects
            Student student1 = new Student("Luis", 22);
            Student student2 = new Student("Mario", 19);
            Student student3 = new Student("James", 20);
            Student student4 = new Student("Ana", 21);


            // A List adds new elements in chronological order
            System.out.println("--List--");

            // A List can be of any kind of data type or objects, so we use generics '<>' to specify the data type we
            // will use
            List<Student> studentsList = new ArrayList<>();

            // Adds objects to the List
            studentsList.add(student1);
            studentsList.add(student2);
            studentsList.add(student3);
            studentsList.add(student4);

            // Prints each element from the List
            for(Student s : studentsList){
                System.out.println(s);
            }


            // A Queue is a FIFO (first in, first out) structure
            System.out.println("--Queue--");

            // I am adding all the elements of my existing list to the Queue, twice
            Queue<Student> studentsQueue = new LinkedList<>(studentsList); // first time
            studentsQueue.addAll(studentsList); // second time

            // While there are elements in the Queue, it removes the first one and prints it
            while(!studentsQueue.isEmpty()){
                System.out.println(studentsQueue.remove());
            }
            // It shows the empty Queue
            System.out.println(studentsQueue);


            // A Set can't hold repeated values
            System.out.println("--Set--");

            // I am adding all the elements in my List to the Set
            Set<Student> studentsSet = new TreeSet<>(studentsList);
            // And then adding the student1 three more times
            studentsSet.add(student1);
            studentsSet.add(student1);
            studentsSet.add(student1);

            // When printing the Set, we noticed it doesn't have repeated values
            for(Student s : studentsSet){
                System.out.println(s);
            }

            // A TreeSet keeps elements sorted and prevents duplicates, but the TreeSet doesn't know how to sort
            // Student objects, so we need to implement the comparable interface to the Student class.
            System.out.println("--Tree--");

            TreeSet<Student> studentsTree = new TreeSet<>();
            studentsTree.add(student1);
            studentsTree.add(student2);
            studentsTree.add(student3);
            studentsTree.add(student4);
            studentsTree.add(student4);
            studentsTree.add(student4);

            // The TreeSet is sorted and has no duplicates
            for(Student student : studentsTree){
                System.out.println(student);
            }
        }catch (Exception e){
            System.out.println("Sorry, something went wrong. Exception: " + e);
        }



    }
}

