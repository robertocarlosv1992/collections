package villanueva;

class Student implements Comparable{
    String name;
    int age;

    /**
     * Parametrized constructor
     * @param name as a string
     * @param age as an integer
     */
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * Determines how to represent the Student object
     * @return a string representing the Student object
     */
    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    /**
     * Determines how to compare the Student objects
     * @param o any type of object
     * @return 1 or -1
     */
    @Override
    public int compareTo(Object o) {
        Student student = (Student)o;
        return (name.compareTo(student.name));
    }
}
